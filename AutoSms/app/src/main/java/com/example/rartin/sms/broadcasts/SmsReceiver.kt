package com.example.rartin.sms.broadcasts

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.telephony.SmsMessage
import android.util.Log
import com.example.rartin.sms.models.SmsModel
import com.example.rartin.sms.network.NetworkManager
import com.google.firebase.iid.FirebaseInstanceId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SmsReceiver(ctx: Context) : BroadcastReceiver() {

    companion object {
        private const val SMS_RECEIVED_INTENT_ACTION = "android.provider.Telephony.SMS_RECEIVED"
        private const val SMS_PDUS = "pdus"
        private const val SMS_FORMAT = "format"
        private const val SMS_SENDER = "+TODO_NUMBER"
        private const val EMPTY_STRING = ""

        lateinit var instance: SmsReceiver
    }

    init {
        instance = this
        ctx.registerReceiver(instance, IntentFilter(SMS_RECEIVED_INTENT_ACTION))
    }

    override fun onReceive(context: Context, intent: Intent) {
        readOtp(intent)?.let { sendOtpToServer(it) }
    }

    private fun sendOtpToServer(otp: String) {
        Log.d(SmsReceiver::class.java.name, "Message: $otp")
        val hash = FirebaseInstanceId.getInstance().id

        NetworkManager().mApis.sendOtp(SmsModel(hash, otp)).enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                Log.d(SmsReceiver::class.java.name, "SendOtp onResponse:$response")
            }

            override fun onFailure(call: Call<Void>?, t: Throwable?) {
                Log.d(SmsReceiver::class.java.name, "SendOtp onFailure:$t")
            }
        })
    }

    private fun readOtp(intent: Intent): String? {
        intent.extras?.let { bundle ->
            val smsMessages: Array<SmsMessage?> = readBundleMessages(bundle)
            if (smsMessages.isNotEmpty()) {
                for (message in smsMessages) {
                    val code = message?.let { retrieveOtpCode(it) } ?: return null
                    if (code.isNotEmpty()) {
                        return code
                    }
                }
            }
        }
        return null
    }

    private fun readBundleMessages(bundle: Bundle): Array<SmsMessage?> {
        val pduMessages: Array<*> = bundle.get(SMS_PDUS) as Array<*>
        val smsMessages = arrayOfNulls<SmsMessage>(pduMessages.size)
        bundle.getString(SMS_FORMAT)?.let {
            for (i in pduMessages.indices) {
                smsMessages[i] = readMessageFromPdu(pduMessages[i], it)
            }
        }
        return smsMessages
    }

    private fun readMessageFromPdu(pduObject: Any?, format: String): SmsMessage? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            SmsMessage.createFromPdu(pduObject as ByteArray, format)
        } else {
            SmsMessage.createFromPdu(pduObject as ByteArray)
        }
    }

    private fun retrieveOtpCode(smsMessage: SmsMessage): String =
            with(smsMessage) {
                val smsBody = this.displayMessageBody
                val sender = this.originatingAddress
                return if (sender != SMS_SENDER) {
                    smsBody
                } else {
                    EMPTY_STRING
                }
            }
}