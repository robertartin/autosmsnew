package com.example.rartin.sms.network

import com.example.rartin.sms.models.RegisterModel
import com.example.rartin.sms.models.SmsModel
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface AutoSmsApis {

    @POST("/registerHash")
    fun registerHash(@Body body: RegisterModel): Call<Void>

    @POST("/sms")
    fun sendOtp(@Body body: SmsModel): Call<Void>
}