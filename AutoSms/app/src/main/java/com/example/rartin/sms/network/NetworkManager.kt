package com.example.rartin.sms.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkManager {
    val mApis: AutoSmsApis

    companion object {
        private const val BASE_URL = "TODO"
    }

    init {
        val autoSmsRetrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        mApis = autoSmsRetrofit.create(AutoSmsApis::class.java)
    }
}