package com.example.rartin.sms.services

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import com.example.rartin.sms.broadcasts.SmsReceiver

class SmsInterceptorService : Service() {

    class SmsInterceptBinder : Binder()

    private var boardCastReceiver: BroadcastReceiver? = null

    override fun onBind(intent: Intent?): IBinder {
        return SmsInterceptBinder()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    override fun onCreate() {
        super.onCreate()
        boardCastReceiver = SmsReceiver(this.applicationContext)
    }
}