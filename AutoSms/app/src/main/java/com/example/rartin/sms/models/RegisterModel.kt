package com.example.rartin.sms.models

data class RegisterModel(val hash: String, val additionalInfo: String)