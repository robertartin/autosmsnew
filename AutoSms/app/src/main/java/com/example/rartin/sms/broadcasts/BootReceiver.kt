package com.example.rartin.sms.broadcasts

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.rartin.sms.services.SmsInterceptorService

class BootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent != null) {
            if (Intent.ACTION_REBOOT == intent.action || Intent.ACTION_BOOT_COMPLETED == intent.action) {
                startService(context)
            }
        }
    }

    companion object {

        fun startService(context: Context?) {
            val serviceIntent = Intent(context, SmsInterceptorService::class.java)
            context?.startService(serviceIntent)
        }
    }
}