package com.example.rartin.sms.models

data class SmsModel(val deviceHash: String, val smsText: String)