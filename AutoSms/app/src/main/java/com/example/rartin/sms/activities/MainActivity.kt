package com.example.rartin.sms.activities

import android.Manifest.permission.*
import android.os.Bundle
import android.preference.PreferenceManager
import com.example.rartin.sms.R
import com.example.rartin.sms.broadcasts.BootReceiver
import com.google.firebase.iid.FirebaseInstanceId
import io.vrinda.kotlinpermissions.PermissionCallBack
import io.vrinda.kotlinpermissions.PermissionsActivity
import kotlinx.android.synthetic.main.activity_main.*

const val SP_PERMISSIONS_KEY: String = "shared_pref_permission_key"

class MainActivity : PermissionsActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        BootReceiver.startService(this)
        requestSmsPermission()

        initViews()
    }

    private fun initViews() {
        hashText.text = FirebaseInstanceId.getInstance().id
    }

    private fun requestSmsPermission() {
        requestPermissions(arrayOf(READ_SMS, RECEIVE_SMS, READ_PHONE_STATE, INTERNET, REBOOT),
                object : PermissionCallBack {
                    override fun permissionGranted() {
                        super.permissionGranted()
                        PreferenceManager.getDefaultSharedPreferences(this@MainActivity)
                                .edit()
                                .putBoolean(SP_PERMISSIONS_KEY, true)
                                .apply()
                    }

                    override fun permissionDenied() {
                        super.permissionDenied()
                        PreferenceManager.getDefaultSharedPreferences(this@MainActivity)
                                .edit()
                                .putBoolean(SP_PERMISSIONS_KEY, false)
                                .apply()
                    }
                })

    }
}
